/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Josermando
 */
@Entity
@Table(name = "nomina")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Nomina.findAll", query = "SELECT n FROM Nomina n"),
    @NamedQuery(name = "Nomina.findById", query = "SELECT n FROM Nomina n WHERE n.id = :id"),
    @NamedQuery(name = "Nomina.findByNombre", query = "SELECT n FROM Nomina n WHERE n.nombre = :nombre"),
    @NamedQuery(name = "Nomina.findByApellido", query = "SELECT n FROM Nomina n WHERE n.apellido = :apellido"),
    @NamedQuery(name = "Nomina.findByCedula", query = "SELECT n FROM Nomina n WHERE n.cedula = :cedula"),
    @NamedQuery(name = "Nomina.findByPosicion", query = "SELECT n FROM Nomina n WHERE n.posicion = :posicion"),
    @NamedQuery(name = "Nomina.findByIdDepartamento", query = "SELECT n FROM Nomina n WHERE n.idDepartamento = :idDepartamento"),
    @NamedQuery(name = "Nomina.findBySueldo", query = "SELECT n FROM Nomina n WHERE n.sueldo = :sueldo"),
    @NamedQuery(name = "Nomina.findByEstado", query = "SELECT n FROM Nomina n WHERE n.estado = :estado"),
    @NamedQuery(name = "Nomina.findByAfp", query = "SELECT n FROM Nomina n WHERE n.afp = :afp"),
    @NamedQuery(name = "Nomina.findBySfs", query = "SELECT n FROM Nomina n WHERE n.sfs = :sfs"),
    @NamedQuery(name = "Nomina.findBySueldoNeto", query = "SELECT n FROM Nomina n WHERE n.sueldoNeto = :sueldoNeto")})
public class Nomina implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "NOMBRE")
    private String nombre;
    @Basic(optional = false)
    @Column(name = "APELLIDO")
    private String apellido;
    @Basic(optional = false)
    @Column(name = "CEDULA")
    private String cedula;
    @Basic(optional = false)
    @Column(name = "POSICION")
    private String posicion;
    @Basic(optional = false)
    @Column(name = "ID_DEPARTAMENTO")
    private int idDepartamento;
    @Basic(optional = false)
    @Column(name = "SUELDO")
    private float sueldo;
    @Basic(optional = false)
    @Column(name = "ESTADO")
    private int estado;
    @Basic(optional = false)
    @Column(name = "AFP")
    private float afp;
    @Basic(optional = false)
    @Column(name = "SFS")
    private float sfs;
    @Basic(optional = false)
    @Column(name = "SUELDO_NETO")
    private float sueldoNeto;

    public Nomina() {
    }

    public Nomina(Integer id) {
        this.id = id;
    }

    public Nomina(Integer id, String nombre, String apellido, String cedula, String posicion, int idDepartamento, float sueldo, int estado, float afp, float sfs, float sueldoNeto) {
        this.id = id;
        this.nombre = nombre;
        this.apellido = apellido;
        this.cedula = cedula;
        this.posicion = posicion;
        this.idDepartamento = idDepartamento;
        this.sueldo = sueldo;
        this.estado = estado;
        this.afp = afp;
        this.sfs = sfs;
        this.sueldoNeto = sueldoNeto;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getPosicion() {
        return posicion;
    }

    public void setPosicion(String posicion) {
        this.posicion = posicion;
    }

    public int getIdDepartamento() {
        return idDepartamento;
    }

    public void setIdDepartamento(int idDepartamento) {
        this.idDepartamento = idDepartamento;
    }

    public float getSueldo() {
        return sueldo;
    }

    public void setSueldo(float sueldo) {
        this.sueldo = sueldo;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public float getAfp() {
        return afp;
    }

    public void setAfp(float afp) {
        this.afp = afp;
    }

    public float getSfs() {
        return sfs;
    }

    public void setSfs(float sfs) {
        this.sfs = sfs;
    }

    public float getSueldoNeto() {
        return sueldoNeto;
    }

    public void setSueldoNeto(float sueldoNeto) {
        this.sueldoNeto = sueldoNeto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Nomina)) {
            return false;
        }
        Nomina other = (Nomina) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "modelo.Nomina[ id=" + id + " ]";
    }
    
}
