/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package manejadores;

import Util.Util;
import dao.empleadoDAO;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.SelectItem;
import modelo.Empleado;

/**
 *
 * @author Josermando
 */
@ManagedBean(name = "empleado")
@SessionScoped
public class empleadoBean {

    private Empleado emp;
    @EJB
    private empleadoDAO dao;
    private List<Empleado> listado;
    private List<Empleado> filtro;
    private boolean updateItem;
    
    public empleadoBean() {
        getEmp();
        emp.setEstado(1);
    }
    
    //Getters and Setters

    public Empleado getEmp() {
        if(emp == null)
            emp = new Empleado();
        return emp;
    }

    public void setEmp(Empleado emp) {
        this.emp = emp;
    }

    public empleadoDAO getDao() {
        return dao;
    }

    public void setDao(empleadoDAO dao) {
        this.dao = dao;
    }

    public List<Empleado> getListado() {
        listado = getDao().buscarTodosEmpleados();
        return listado;
    }

    public void setListado(List<Empleado> listado) {
        this.listado = listado;
    }

    public boolean isUpdateItem() {
        return updateItem;
    }

    public void setUpdateItem(boolean isEditable) {
        this.updateItem = isEditable;
    }

    public List<Empleado> getFiltro() {
        return filtro;
    }

    public void setFiltro(List<Empleado> filtro) {
        this.filtro = filtro;
    }
    
    //Metodos CRUD
    
    public String guardarEmpleado(){
       getDao().guardar(emp);
       setUpdateItem(false);
       clear();
       return "/empleado.xhtml?faces-redirect=true";
    }
    public void actualizar(){
        getDao().actualizar(emp);
        setUpdateItem(false);
        clear();
    }
    public void actualizarEmpleado(Empleado emp){
        this.emp = emp;
        setUpdateItem(true);
        System.out.println("Funciono en Empleado?");
    } 
    
    public void eliminarEmpleado(Empleado emp){
        this.emp = emp;
        setUpdateItem(true);
        eliminar();
        System.out.println("Eliminado");
    }
    
    public void eliminar(){
        getDao().suspender(emp);
        getDao().actualizar(emp);
        setUpdateItem(false);
        clear();
    }
    
    public void clear(){
        emp = null;
    }
    
    
    public SelectItem[] getItemsAvailableSelectMany() {
        return Util.getSelectItems(getDao().buscarTodos(), false);
    }
     
     @FacesConverter(forClass = Empleado.class)
    public static class EmpleadoConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            empleadoBean controller = (empleadoBean) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "empleadoBean");
            return controller.getDao().buscar(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Empleado) {
                Empleado o = (Empleado) object;
                return getStringKey(o.getId());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + Empleado.class.getName());
            }
        }

    }
    
    
}
