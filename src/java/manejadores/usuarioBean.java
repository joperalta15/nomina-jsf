/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package manejadores;

import Util.Util;
import dao.usuarioDAO;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import modelo.Usuario;

/**
 *
 * @author Josermando
 */
@ManagedBean(name = "usuario")
@SessionScoped
public class usuarioBean {

    private Usuario user;
    @EJB
    private usuarioDAO dao;
    private List<Usuario> listado;
    private String username;
    private String password;
    private boolean loggedIn = false;
    public usuarioBean() {
    }
    //Getters & Setters

    public Usuario getUser() {
        return user;
    }

    public void setUser(Usuario user) {
        this.user = user;
    }

    public usuarioDAO getDao() {
        return dao;
    }

    public void setDao(usuarioDAO dao) {
        this.dao = dao;
    }

    public List<Usuario> getListado() {
        return listado;
    }

    public void setListado(List<Usuario> listado) {
        this.listado = listado;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isLoggedIn() {
        return loggedIn;
    }

    public void setLoggedIn(boolean loggedIn) {
        this.loggedIn = loggedIn;
    }
    
    public void verificarAuth(){
        if(!this.loggedIn){
            doRedirect("login.xhtml");
        }
    }
    
    public void doRedirect(String url){
        try{
            FacesContext fc = FacesContext.getCurrentInstance();
        fc.getExternalContext().redirect(url);
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
    }
    //Metodo autenticar
    public String autenticar(){
        
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
        user = getDao().validarUsuario(getUsername(), getPassword());
        //Login exitoso
        if(user!=null){
            setLoggedIn(true);
            return "/index.xhtml";
        }
        //Login fallido
        else{
            FacesMessage msg = new FacesMessage("Datos erroneos", "ERROR_MSG");
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            FacesContext.getCurrentInstance().addMessage(null, msg);
            setLoggedIn(false);
            return "/login.xhtml";
        }
    }
    public String logout(){
        HttpSession sesion = Util.getSession();
        sesion.invalidate();
        setLoggedIn(false);
        return "/login.xhtml";
    }
    
}
