/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package manejadores;

import Util.Util;
import dao.departamentoDAO;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.SelectItem;
import modelo.Departamento;

/**
 *
 * @author Josermando
 */
@ManagedBean(name = "departamento")
@SessionScoped
public class departamentoBean {

    private Departamento depto;
    @EJB
    private departamentoDAO dao;
    private List<Departamento> listado;
    public boolean updateItem;
    
    public departamentoBean() {
        getDepto();
        depto.setEstado(1);
    }
//    @PostConstruct
//    public void init(){
//        depto.setEstado(1);
//    }
//    
    //Getters and Setters

    public Departamento getDepto() {
        if(depto == null)
            depto = new Departamento();
        return depto;
    }

    public void setDepto(Departamento depto) {
        this.depto = depto;
    }

    public departamentoDAO getDao() {
        return dao;
    }

    public void setDao(departamentoDAO dao) {
        this.dao = dao;
    }

    public List<Departamento> getListado() {
        listado = getDao().buscarTodosDeptos();
        return listado;
    }

    public void setListado(List<Departamento> listado) {
        this.listado = listado;
    }

    public boolean isUpdateItem() {
        return updateItem;
    }

    public void setUpdateItem(boolean isEditable) {
        this.updateItem = isEditable;
    }
    
    
    
    //Metodos CRUD
    public String guardarDepto(){
        getDao().guardar(depto);
        setUpdateItem(false);
        clear();
        return "/departamento.xhtml?faces-redirect=true";
    }
    
    public void actualizar(){
        getDao().actualizar(depto);
        setUpdateItem(false);
        clear();
    }
    
    public void actualizarDepto(Departamento depto){
        this.depto = depto;
        setUpdateItem(true);
        System.out.println("Funciono??");
    }
    
    public void eliminarDepto(Departamento depto){
        this.depto = depto;
        setUpdateItem(true);
        eliminar();
        System.out.println("Eliminando");
    }
    
    public void eliminar(){
        getDao().suspender(depto);
        getDao().actualizar(depto);
        setUpdateItem(false);
        clear();
    }
    
    public void clear(){
        depto = null;
    }
    public SelectItem[] getItemsAvailableSelectOne() {
        return Util.getSelectItems(getDao().buscarTodos(), true);
    }
    
     public SelectItem[] getItemsAvailableSelectMany() {
        return Util.getSelectItems(getDao().buscarTodos(), false);
    }
     
    @FacesConverter(forClass = Departamento.class)
    public static class DepartamentoConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            departamentoBean controller = (departamentoBean) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "departamento");
            return controller.getDao().buscar(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Departamento) {
                Departamento o = (Departamento) object;
                return getStringKey(o.getId());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + Departamento.class.getName());
            }
        }

    }
}
