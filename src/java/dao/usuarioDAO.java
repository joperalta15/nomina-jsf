/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import modelo.Usuario;

/**
 *
 * @author Josermando
 */
@Stateless
public class usuarioDAO extends AbstractDAO{
    @PersistenceContext(unitName = "NominaJSFPU")
    private EntityManager em;
    public usuarioDAO() {
        super(Usuario.class);
    }

    @Override
    protected EntityManager getEM() {
        return em;
    }
    public Usuario validarUsuario(String user, String pass){
        TypedQuery<Usuario> query = getEM().createNamedQuery("Usuario.findByUser&Pass", Usuario.class)
                .setParameter("username", user)
                .setParameter("password", pass);
        Usuario usuario = query.getResultList().get(0);
        return usuario;
    }
}
