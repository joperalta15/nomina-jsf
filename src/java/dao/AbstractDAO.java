/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaQuery;

/**
 *
 * @author Josermando
 */
public abstract class AbstractDAO<T> {
    private Class<T> entity;
    protected abstract EntityManager getEM();
    
    public AbstractDAO(Class<T> entity) {
        this.entity = entity;
    }
    
    public void guardar(T entity){
        getEM().persist(entity);
    }
    
    public void actualizar(T entity){
        getEM().merge(entity);
    }
    
    public void eliminar(T entity){
        getEM().remove(getEM().merge(entity));
    }
    
    public T buscar(Object id){
        return getEM().find(entity, id);
    }
    
    public List<T> buscarTodos(){
        CriteriaQuery cq = getEM().getCriteriaBuilder().createQuery();
        cq.select(cq.from(entity));
        return getEM().createQuery(cq).getResultList();
    }
}
