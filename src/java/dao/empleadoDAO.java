/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import modelo.Empleado;

/**
 *
 * @author Josermando
 */
@Stateless
public class empleadoDAO extends AbstractDAO {
    @PersistenceContext(unitName = "NominaJSFPU")
    private EntityManager em;
    
    public empleadoDAO() {
        super(Empleado.class);
    }

    @Override
    protected EntityManager getEM() {
        return em;
    }
    
    public void suspender(Empleado emp){
        emp.setId(2);
    }
    
    public List<Empleado> buscarTodosEmpleados(){
        TypedQuery<Empleado> query = getEM().createNamedQuery("Empleado.findByEstado", Empleado.class);
        List<Empleado> resultados = query.getResultList();
        return resultados;
    }
}
